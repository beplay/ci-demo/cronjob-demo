#!/bin/sh

#########################################
# This example is main execution file
#########################################

echo "Job begining.."

echo -e "Listing directory /data \n $(ls -l /data/)"
echo -e "Listing directory $(pwd) \n $(ls -l)"

echo "Job completed."

