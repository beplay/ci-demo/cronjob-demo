FROM alpine

RUN mkdir -p /app

COPY . /app
WORKDIR /app

ENTRYPOINT [ "/app/run.sh" ]